package MainApp;

public interface NRFDebugInfoProvider {
    String getDebugInfo();
}
