package MainApp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;

public class RoverDataParser {
    private RoverData data;
    private RoverDataSerializer serializer;
    private StringProperty serializedData;

    RoverDataParser() {
        serializer = (data) -> "No serializer specified";
        serializedData = new SimpleStringProperty();
    }

    public void setSerializer(RoverDataSerializer serializer) {
        this.serializer = serializer;
    }

    public RoverData getData() {
        return data;
    }

    public void setData(RoverData data) {
        this.data = data;
    }

    public String getSerializedData() {
        return serializedData.getValue();
    }

    public void serialize() {
        serializedData.setValue(serializer.serialize(data));
    }

    public void addSerializationListener(ChangeListener<String> listener) {
        serializedData.addListener(listener);
    }
}
