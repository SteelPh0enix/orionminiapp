package MainApp;

public interface RoverDataSerializer {
    String serialize(RoverData data);
}
