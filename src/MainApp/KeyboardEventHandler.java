package MainApp;

import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyboardEventHandler {
    private KeyWithState forwardKey = new KeyWithState(KeyCode.W);
    private KeyWithState backwardKey = new KeyWithState(KeyCode.S);
    private KeyWithState rotateLeftKey = new KeyWithState(KeyCode.A);
    private KeyWithState rotateRightKey = new KeyWithState(KeyCode.D);

    private EventHandler<KeyEvent> onPressedHandler = event -> {
        KeyCode pressedKey = event.getCode();
        if (pressedKey == forwardKey.getCode()) {
            forwardKey.setPressed(true);
        } else if (pressedKey == backwardKey.getCode()) {
            backwardKey.setPressed(true);
        } else if (pressedKey == rotateLeftKey.getCode()) {
            rotateLeftKey.setPressed(true);
        } else if (pressedKey == rotateRightKey.getCode()) {
            rotateRightKey.setPressed(true);
        }
    };

    private EventHandler<KeyEvent> onReleasedHandler = event -> {
        KeyCode pressedKey = event.getCode();
        if (pressedKey == forwardKey.getCode()) {
            forwardKey.setPressed(false);
        } else if (pressedKey == backwardKey.getCode()) {
            backwardKey.setPressed(false);
        } else if (pressedKey == rotateLeftKey.getCode()) {
            rotateLeftKey.setPressed(false);
        } else if (pressedKey == rotateRightKey.getCode()) {
            rotateRightKey.setPressed(false);
        }
    };

    public EventHandler<KeyEvent> getOnPressedHandler() {
        return onPressedHandler;
    }

    public EventHandler<KeyEvent> getOnReleasedHandler() {
        return onReleasedHandler;
    }

    public KeyWithState getForwardKey() {
        return forwardKey;
    }

    public void setForwardKey(KeyWithState forwardKey) {
        this.forwardKey = forwardKey;
    }

    public KeyWithState getBackwardKey() {
        return backwardKey;
    }

    public void setBackwardKey(KeyWithState backwardKey) {
        this.backwardKey = backwardKey;
    }

    public KeyWithState getRotateLeftKey() {
        return rotateLeftKey;
    }

    public void setRotateLeftKey(KeyWithState rotateLeftKey) {
        this.rotateLeftKey = rotateLeftKey;
    }

    public KeyWithState getRotateRightKey() {
        return rotateRightKey;
    }

    public void setRotateRightKey(KeyWithState rotateRightKey) {
        this.rotateRightKey = rotateRightKey;
    }

    public void addGlobalListener(ChangeListener<Boolean> listener) {
        getForwardKey().pressedProperty().addListener(listener);
        getBackwardKey().pressedProperty().addListener(listener);
        getRotateLeftKey().pressedProperty().addListener(listener);
        getRotateRightKey().pressedProperty().addListener(listener);
    }
}
