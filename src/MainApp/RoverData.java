package MainApp;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;

public class RoverData {
    private DoubleProperty speed;
    private DoubleProperty rotation;

    private DoubleProperty speedPercentage;
    private DoubleProperty rotationPercentage;

    private DoubleProperty speedLimit;
    private DoubleProperty rotationLimit;

    private double hardSpeedLimit;
    private double hardRotationLimit;

    public RoverData(double hardSpeedLimit, double hardRotationLimit) {
        speed = new SimpleDoubleProperty(0.);
        rotation = new SimpleDoubleProperty(0.);
        speedPercentage = new SimpleDoubleProperty(0.);
        rotationPercentage = new SimpleDoubleProperty(0.);
        speedLimit = new SimpleDoubleProperty(hardSpeedLimit);
        rotationLimit = new SimpleDoubleProperty(hardRotationLimit);

        this.hardRotationLimit = hardRotationLimit;
        this.hardSpeedLimit = hardSpeedLimit;
    }

    private void recalculateSpeed() {
        speedPercentageProperty().set((getSpeed() + hardSpeedLimit) / (hardSpeedLimit * 2));
    }

    private void recalculateRotation() {
        rotationPercentageProperty().set((getRotation() + hardRotationLimit) / (hardRotationLimit * 2));
    }

    public double getSpeed() {
        return speed.get();
    }

    public void setSpeed(double speed) {
        this.speed.set(speed);
        recalculateSpeed();
    }

    public DoubleProperty speedProperty() {
        return speed;
    }

    public double getRotation() {
        return rotation.get();
    }

    public void setRotation(double rotation) {
        this.rotation.set(rotation);
        recalculateRotation();
    }

    public DoubleProperty rotationProperty() {
        return rotation;
    }

    public double getSpeedLimit() {
        return speedLimit.get();
    }

    public void setSpeedLimit(double speedLimit) {
        this.speedLimit.set(speedLimit);
        recalculateSpeed();
    }

    public DoubleProperty speedLimitProperty() {
        return speedLimit;
    }

    public double getRotationLimit() {
        return rotationLimit.get();
    }

    public void setRotationLimit(double rotationLimit) {
        this.rotationLimit.set(rotationLimit);
        recalculateRotation();
    }

    public DoubleProperty rotationLimitProperty() {
        return rotationLimit;
    }

    public double getSpeedPercentage() {
        return speedPercentage.get();
    }

    public DoubleProperty speedPercentageProperty() {
        return speedPercentage;
    }

    public double getRotationPercentage() {
        return rotationPercentage.get();
    }

    public DoubleProperty rotationPercentageProperty() {
        return rotationPercentage;
    }

    public void addDataChangedListener(ChangeListener<Number> listener) {
        speedProperty().addListener(listener);
        rotationProperty().addListener(listener);
    }
}
