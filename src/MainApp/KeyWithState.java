package MainApp;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.input.KeyCode;

public class KeyWithState {

    private KeyCode code;
    private BooleanProperty pressed = new SimpleBooleanProperty(false);

    public KeyWithState(KeyCode keyCode) {
        setCode(keyCode);
    }

    public KeyCode getCode() {
        return code;
    }

    public void setCode(KeyCode code) {
        this.code = code;
    }

    public boolean isPressed() {
        return pressed.get();
    }

    public void setPressed(boolean pressed) {
        this.pressed.set(pressed);
    }

    public BooleanProperty pressedProperty() {
        return pressed;
    }
}
