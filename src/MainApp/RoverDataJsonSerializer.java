package MainApp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/*
    JSON format:
    {"CSPD": speed, "CTRN": rotation}
*/

public class RoverDataJsonSerializer implements RoverDataSerializer {
    private final String SpeedJsonKey = "CSPD";
    private final String RotationJsonKey = "CTRN";

    RoverDataJsonSerializer() {
    }

    public String serialize(RoverData data) {
        JsonObject json = new JsonObject();
        json.add(SpeedJsonKey, new JsonPrimitive(data.speedProperty().intValue()));
        json.add(RotationJsonKey, new JsonPrimitive(data.rotationProperty().intValue()));
        Gson gson = new GsonBuilder().create();
        return gson.toJson(json);
    }
}
