package MainApp;

import com.fazecast.jSerialComm.SerialPort;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class MainUIController implements Initializable {

    @FXML
    private TextArea textLog;
    @FXML
    private ComboBox<SerialPort> comboSerialPort;
    @FXML
    private ComboBox<String> comboInputSource;
    @FXML
    private TextField inputBaudRate;
    @FXML
    private ProgressBar progressRotationIndicator;
    @FXML
    private ProgressBar progressSpeedIndicator;
    @FXML
    private Slider sliderRotationLimit;
    @FXML
    private Slider sliderSpeedLimit;
    @FXML
    private CheckBox checkboxShowSerialOutput;
    @FXML
    private CheckBox checkboxShowFeedback;
    private ObservableList<SerialPort> serialPortListModel;
    private ObservableList<String> inputMethodsListModel;
    private IntegerProperty baudRate;
    private Property<SerialPort> serialPort;
    private StringProperty inputMethod;
    private NRFDebugInfoProvider nrfDebugInfoProvider;

    public MainUIController() {
        serialPortListModel = FXCollections.observableArrayList();
        inputMethodsListModel = FXCollections.observableArrayList();
        serialPort = new SimpleObjectProperty<>();
        inputMethod = new SimpleStringProperty("");
        nrfDebugInfoProvider = () -> "No NRF debug info provider available";
    }

    @Override
    public void initialize(URL url, ResourceBundle res) {
        // Prevent non-numeric input in baud rate text field
        inputBaudRate.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                inputBaudRate.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        baudRate = new SimpleIntegerProperty(getRawBaudRate());
    }

    public void log(String text) {
        textLog.appendText(text + "\n");
    }

    public void setNrfDebugInfoProvider(NRFDebugInfoProvider nrfDebugInfoProvider) {
        this.nrfDebugInfoProvider = nrfDebugInfoProvider;
    }

    public ObservableList<SerialPort> getSerialPortListModel() {
        return serialPortListModel;
    }

    public void setSerialPortListModel(ObservableList<SerialPort> model) {
        serialPortListModel = model;
        comboSerialPort.setItems(serialPortListModel);
    }

    public ObservableList<String> getInputMethodsListModel() {
        return inputMethodsListModel;
    }

    public void setInputMethodsListModel(ObservableList<String> model) {
        inputMethodsListModel = model;
        comboInputSource.setItems(inputMethodsListModel);
        if (model.size() > 0) {
            getInputMethodProperty().setValue(model.get(0));
            log("Selected input device: " + getInputMethodProperty().getValue());
        }
    }

    public double getSpeedLimit() {
        return sliderSpeedLimit.getValue();
    }

    public double getRotationLimit() {
        return sliderRotationLimit.getValue();
    }

    public SerialPort getSerialPort() {
        return serialPort.getValue();
    }

    public String getInputMethod() {
        return inputMethod.get();
    }

    public int getBaudRate() {
        return baudRate.getValue();
    }

    public boolean getIsFeedbackEnabled() {
        return checkboxShowFeedback.isSelected();
    }

    public boolean getIsSerialOutputEnabled() {
        return checkboxShowSerialOutput.isSelected();
    }

    public DoubleProperty getSpeedLimitProperty() {
        return sliderSpeedLimit.valueProperty();
    }

    public DoubleProperty getRotationLimitProperty() {
        return sliderRotationLimit.valueProperty();
    }

    public DoubleProperty getRotationIndicatorProgressProperty() {
        return progressRotationIndicator.progressProperty();
    }

    public DoubleProperty getSpeedIndicatorProgressProperty() {
        return progressSpeedIndicator.progressProperty();
    }

    public IntegerProperty getBaudRateProperty() {
        return baudRate;
    }

    public Property<SerialPort> getSerialPortProperty() {
        return serialPort;
    }

    public StringProperty getInputMethodProperty() {
        return inputMethod;
    }

    public BooleanProperty getIsFeedbackEnabledProperty() {
        return checkboxShowFeedback.selectedProperty();
    }

    public BooleanProperty getIsSerialOutputEnabledProperty() {
        return checkboxShowSerialOutput.selectedProperty();
    }

    @FXML
    private void inputDeviceChanged() {
        log("Input device changed to " + comboInputSource.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void serialPortChanged() {
        SerialPort selected = comboSerialPort.getSelectionModel().getSelectedItem();
        log("Serial port changed to " + selected.getSystemPortName() + " - " + selected.getDescriptivePortName());
        serialPort.setValue(selected);
    }

    @FXML
    private void baudRateChanged() {
        log("Baud rate changed to " + inputBaudRate.getText());
        baudRate.setValue(getRawBaudRate());
    }

    @FXML
    private void printNRFDebug() {
        log(nrfDebugInfoProvider.getDebugInfo());
    }

    @FXML
    private void clearLog() {
        textLog.clear();
    }

    @FXML
    private void refreshSerialPorts() {
        log("Serial ports list refreshed");
        serialPortListModel.setAll(SerialPort.getCommPorts());
    }

    private int getRawBaudRate() {
        return Integer.parseInt(inputBaudRate.getText());
    }
}
