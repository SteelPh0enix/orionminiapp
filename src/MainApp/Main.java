package MainApp;

import com.fazecast.jSerialComm.SerialPort;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        KeyboardEventHandler keyHandler = new KeyboardEventHandler();
        RoverData roverData = new RoverData(180., 180.);
        RoverDataParser dataParser = new RoverDataParser();
        RoverSerialCommunicator serialCommunicator = new RoverSerialCommunicator(100, 115200);

        dataParser.setSerializer(new RoverDataJsonSerializer());
        dataParser.setData(roverData);
        dataParser.addSerializationListener(
                (observable, oldValue, newValue) -> serialCommunicator.setDataOutput(newValue));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));

        Parent root = loader.load();
        Scene mainScene = new Scene(root, 800, 600);

        primaryStage.setTitle("Orion Mini control panel");
        primaryStage.setResizable(true);

        MainUIController controller = prepareController(loader);

        mainScene.setOnKeyPressed(keyHandler.getOnPressedHandler());
        mainScene.setOnKeyReleased(keyHandler.getOnReleasedHandler());

        bindControllerToRoverData(controller, roverData);
        bindRoverDataToKeyboard(roverData, keyHandler);
        bindRoverCommunicatorToRoverData(dataParser, roverData);

        // Adds JSON logging functionality
        roverData.addDataChangedListener(((observable, oldValue, newValue) -> {
            if (controller.getIsSerialOutputEnabledProperty().getValue()) {
                controller.log(dataParser.getSerializedData());
            }
        }));

        controller.setNrfDebugInfoProvider(serialCommunicator.getNRFDebugInfoProvider());

        controller.getSerialPortProperty().addListener((observable, oldValue, newValue) -> {
            serialCommunicator.setPort(newValue);
            if (oldValue == null) {
                serialCommunicator.start();
                controller.log("Starting...");
            }
        });

        primaryStage.setOnCloseRequest((event ->
                serialCommunicator.stop()
        ));

        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    private void bindRoverDataToKeyboard(RoverData data, KeyboardEventHandler handler) {
        handler.getForwardKey().pressedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                data.setSpeed(data.getSpeedLimit());
            } else {
                if (handler.getBackwardKey().isPressed()) {
                    data.setSpeed(-data.getSpeedLimit());
                } else {
                    data.setSpeed(0);
                }
            }
        });

        handler.getBackwardKey().pressedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                data.setSpeed(-data.getSpeedLimit());
            } else {
                if (handler.getForwardKey().isPressed()) {
                    data.setSpeed(data.getSpeedLimit());
                } else {
                    data.setSpeed(0);
                }
            }
        });

        handler.getRotateRightKey().pressedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                data.setRotation(data.getRotationLimit());
            } else {
                if (handler.getRotateLeftKey().isPressed()) {
                    data.setRotation(-data.getRotationLimit());
                } else {
                    data.setRotation(0);
                }
            }
        });

        handler.getRotateLeftKey().pressedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                data.setRotation(-data.getRotationLimit());
            } else {
                if (handler.getRotateRightKey().isPressed()) {
                    data.setRotation(data.getRotationLimit());
                } else {
                    data.setRotation(0);
                }
            }
        });
    }

    private void bindRoverCommunicatorToRoverData(RoverDataParser communicator, RoverData data) {
        data.addDataChangedListener(((observable, oldValue, newValue) -> {
            communicator.serialize();
        }));
    }

    private MainUIController prepareController(FXMLLoader loader) {
        MainUIController controller = loader.getController();
        controller.setSerialPortListModel(FXCollections.observableArrayList(SerialPort.getCommPorts()));
        controller.setInputMethodsListModel(FXCollections.observableArrayList("Keyboard"));
        return controller;
    }

    private void bindControllerToRoverData(MainUIController controller, RoverData roverData) {
        controller.getRotationIndicatorProgressProperty().bind(roverData.rotationPercentageProperty());
        controller.getSpeedIndicatorProgressProperty().bind(roverData.speedPercentageProperty());

        roverData.speedLimitProperty().bind(controller.getSpeedLimitProperty());
        roverData.rotationLimitProperty().bind(controller.getRotationLimitProperty());
    }
}
