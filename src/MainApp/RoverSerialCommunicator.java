package MainApp;

import com.fazecast.jSerialComm.SerialPort;

import java.util.concurrent.TimeUnit;

public class RoverSerialCommunicator {
    private final int RoverBaudRate;
    private SerialPort roverSerialPort;
    private NRFDebugInfoProvider nrfDebugInfoProvider;
    private Thread dataSenderThread;
    private boolean running;
    private String data;
    private int commsDelay;
    private Runnable dataSender;

    RoverSerialCommunicator(int commsDelay, int baudRate) {
        RoverBaudRate = baudRate;
        nrfDebugInfoProvider = () -> getNRFDebugInfo();
        running = false;
        this.commsDelay = commsDelay;
        data = "";

        dataSender = () -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                return;
            }

            while (true) {
                try {
                    byte[] writeBuffer = (data + '\n').getBytes();
                    long writeBufferLength = writeBuffer.length;
                    System.out.println("Writing " + writeBufferLength + " bytes (" + new String(writeBuffer) + ") to AVR");
                    roverSerialPort.writeBytes(writeBuffer, writeBufferLength);

                    int waitingToRead = roverSerialPort.bytesAvailable();
                    System.out.println("Waiting to be readed: " + waitingToRead);
                    byte[] readBuffer = new byte[waitingToRead];
                    roverSerialPort.readBytes(readBuffer, waitingToRead);
                    System.out.println("Readed data: " + new String(readBuffer));

                    TimeUnit.MILLISECONDS.sleep(commsDelay);
                } catch (InterruptedException e) {
                    break;
                }
            }
        };
    }

    public void setPort(SerialPort port) {
        boolean wasRunning = running;
        if (dataSenderThread != null && dataSenderThread.isAlive()) {
            stop();
        }
        if (roverSerialPort != null) {
            roverSerialPort.closePort();
        }


        port.setComPortParameters(RoverBaudRate, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
        port.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING | SerialPort.TIMEOUT_READ_BLOCKING, 500,500);
        port.openPort();
        roverSerialPort = port;
        System.out.println("Serial port opened");
        System.out.println(port.getPortDescription());
        System.out.println(port.getBaudRate());

        if (wasRunning) {
            start();
        }
    }

    public void setDataOutput(String data) {
        this.data = data;
    }

    public NRFDebugInfoProvider getNRFDebugInfoProvider() {
        return nrfDebugInfoProvider;
    }

    public void start() {
        running = true;
        dataSenderThread = new Thread(dataSender);
        dataSenderThread.start();
    }

    public void stop() {
        running = false;
        if (dataSenderThread != null) {
            dataSenderThread.interrupt();
        }
    }

    private String getNRFDebugInfo() {
        return "Not implemented yet";
    }
}
